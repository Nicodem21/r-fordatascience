Games

row.names(Games)
colnames(Games)

Games["LeBronJames", "2012"]

FieldGoals 

round(FieldGoals / Games, 1)


round(MinutesPlayed / Games)

MinutesPlayed


# on veux afficher chaque avec une couleur unique
?matplot
FieldGoals
t(FieldGoals) # on transpose la matrice
FieldGoals
matplot(t(FieldGoals/Games), type ="b", pch = 15:18, col = c(1:4, 6))
legend("bottomleft", inset = 0.01, legend=Players, col = c(1:4, 6), 
       pch=15:18, horiz = F)

matplot(t(FieldGoals/FieldGoalAttempts), type ="b", pch = 15:18, col = c(1:4, 6))
legend("bottomleft", inset = 0.01, legend=Players, col = c(1:4, 6), 
       pch=15:18, horiz = F)

Games[1,, drop=F] # drop=F, permet d'affiche la gine avec son étiquette 
