
?read.csv()

# Method 1: select the file Manually
stats <- read.csv(file.choose())
stats


# Method 2; set working directory and read data
getwd()
# Windows
setwd("C;\\Users\\brice21\\Desktop\\R Programming")

# mac os
setwd("/Users/brice21/Desktop/farmationR/section5")
getwd()

rm(stats)
stats <- read.csv("P2-Demographic-Data.csv")
stats

#--------------------------------------------Exploring Data
stats
# Nombre de lignes et de colonnes de notre dataset
nrow(stats) 
ncol(stats)

# Affiche les premières lignes
head(stats, n=10)
# Affiche les dernières lignes
tail(stats, n=8)
# Affiche la structure du jeu de données
str(stats) # runif()
runif(stats)
# Un resumé rapide du jeu de données
summary(stats)

#--------------------------------------- using the $ sign
stats
head(stats)
stats[3,3]
stats[3,"Birth.rate"]

stats$Internet.users
stats$Internet.users[2]
levels((stats$Income.Group))

#--------------------------------------- Basic operation with a DF
stats[1:10, ] # subsetting
stats[3:9, ]
stats[c(4,100)]

# Remember how the [] work
is.data.frame(stats[1,])# pas besoin de fixer drop=F (déjà fait par défaut)
is.data.frame(stats[,1])# n'est pas un DF
is.data.frame(stats[,1, drop=F])# est un DF

# Multiply columns
head(stats)
stats$Birth.rate * stats$Internet.users
stats$Birth.rate + stats$Internet.users

# Add columns
head(stats)
stats$Mycolc <- stats$Birth.rate * stats$Internet.users

# remove column
stats$Mycolc <- NULL

#---------------------------------------  Filtering Data Frames
head(stats)
filter <- stats$Internet.users < 2
filter
stats[filter,]

stats[stats$Birth.rate > 40,]
stats[stats$Birth.rate > 40 & stats$Internet.users < 2,]
stats[stats$Income.Group == "High income", ]
levels((stats$Income.Group))

stats[stats$Country.Name == "Malta",]

#--------------------------------------- Introduction to qplot()
# install.packages("ggplot2")
library(ggplot2)
?qplot
head(stats)
qplot(data = stats, x=Internet.users)
qplot(data = stats, x=Income.Group, y=Birth.rate)
qplot(data = stats, x=Income.Group, y=Birth.rate, size=I(3),
      color=I("blue"))
qplot(data = stats, x=Income.Group, y=Birth.rate, geom="boxplot")

#---------------------------------------  Visualizing what we need
qplot(data = stats, x=Internet.users, y=Birth.rate )
qplot(data = stats, x=Internet.users, y=Birth.rate,
      color = I("red"), size=I(2))
qplot(data = stats, x=Internet.users, y=Birth.rate,
      color = Income.Group, size=I(2))

#--------------------------------------- Creating Data Frame
mydf <- data.frame(Countries_2012_Dataset, Codes_2012_Dataset, 
                   Regions_2012_Dataset)
head(mydf)  
#colnames(mydf) <- c("Country","Code", "Region")

rm(mydf)
mydf <- data.frame(Country=Countries_2012_Dataset, Code=Codes_2012_Dataset, 
                   Region=Regions_2012_Dataset)
summary(mydf)

#---------------------------------------  Mergind Data Frames
head(mydf)
head(stats)
merged <- merge(stats, mydf, by.x = "Country.Code", by.y = "Code")
head(merged)

merged$Country <- NULL # on supprime la colonne "Country"
str(merged)

#--------------------------------------- Visualizing with new split
qplot(data = merged, x=Internet.users , y=Birth.rate)
# shape
qplot(data = merged, x=Internet.users , y=Birth.rate, 
      colour=Region, size=I(5), shape=I(23))
# Transparency
qplot(data = merged, x=Internet.users , y=Birth.rate, 
      colour=Region, size=I(5), shape=I(23), alpha=I(0.6))
#Title
qplot(data = merged, x=Internet.users , y=Birth.rate, 
      colour=Region, size=I(2), shape=I(19), alpha=I(0.6),
      main = "Birth Rate vs Internet Users")