

counter <- 1
while(counter < 12){
  print(counter)
  counter <- counter + 1
}

# 1:5 est un vecteur
for (i in 1:5) {
  print("Hello R")
}

for (i in 5:10) {
  print("Hello R")
}

# ------- -2 ----- -1 ----- 0 ----- 1 -----2-----
rm(answer)
x <- rnorm(1)
if(x>1){
 answer <- "Greater than 1" 
}else{
  answer <- "Less or equal to 1"  
}
